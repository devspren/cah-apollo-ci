image: node:erbium

variables:
  APP_NAME: cah-apollo
  HEROKU_APP_PIPELINE: cah-apollo-pipeline
  HEROKU_APP_PRODUCTION: cah-apollo
  HEROKU_APP_STAGING: cah-apollo-staging
  HEROKU_DEFAULT_DEV_APP: cah-apollo-development
  REVIEW_APP_NAME: '$APP_NAME-$CI_COMMIT_BRANCH'
  REVIEW_REMOTE: 'heroku-$CI_COMMIT_BRANCH'
  STAGING_REMOTE: 'staging'
  PRODUCTION_REMOTE: 'production'
  DEVELOPMENT_REMOTE: 'development'
  GIT_CI_REPO: 'https://gitlab.com/devspren/cah-apollo-ci.git'

before_script:
  # Get ci/cd scripts
  - |
    if ! ls ci &> /dev/null;
      then
        git clone $GIT_CI_REPO ci;
    fi;
  - |
    if [[ $CI_JOB_STAGE == 'reviewing' || $CI_JOB_STAGE =~ ^staging:* || $CI_JOB_STAGE == 'production' ]];
      then
        apt-get update -qy
        # Install Heroku CLI
          if ! command -v heroku &> /dev/null;
            then curl https://cli-assets.heroku.com/install-ubuntu.sh | sh;
          fi
        # Install Expect
          if ! command -v expect &> /dev/null;
            then apt-get install -y expect;
          fi
        # Heroku login
        expect ci/login-heroku.exp &> /dev/null;        
    fi;  

after_script:
  - |
    if [[ $CI_JOB_STAGE == 'reviewing' || $CI_JOB_STAGE == 'staging' || $CI_JOB_STAGE == 'production' ]];
      then
        # Heroku logout
        heroku logout        
    fi;

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
    - ci/

stages:
  - testing
  - reviewing
  - staging:staging_app
  - staging:default_dev_app
  - production

testing_job:
  stage: testing
  variables:
    AUTHO_MANAGEMENT_API_CLIENT_ID: '$DEVELOPMENT_AUTHO_MANAGEMENT_API_CLIENT_ID'
    AUTHO_MANAGEMENT_API_CLIENT_SECRET: '$DEVELOPMENT_AUTHO_MANAGEMENT_API_CLIENT_SECRET'
  script:
    - echo 'Runing Test'
    - npm install 
    - npm run test

reviewing_job:
  stage: reviewing
  only: 
    - branches
  except: 
    - master
  variables:
    NODE_ENV: development
    AUTHO_MANAGEMENT_API_CLIENT_ID: '$DEVELOPMENT_AUTHO_MANAGEMENT_API_CLIENT_ID'
    AUTHO_MANAGEMENT_API_CLIENT_SECRET: '$DEVELOPMENT_AUTHO_MANAGEMENT_API_CLIENT_SECRET'
  script: 
    # --- New Review app ---
    # Create a new Heroku app and his remote if it doesn't already exist
    - |
      if ! heroku apps:info -a $REVIEW_APP_NAME &> /dev/null; 
        then 
          heroku create $REVIEW_APP_NAME -r $REVIEW_REMOTE && 
          # Add the app to pipeline in development stage
          heroku pipelines:add $HEROKU_APP_PIPELINE -a $REVIEW_APP_NAME -s development;
        else 
          heroku git:remote -a $REVIEW_APP_NAME -r $REVIEW_REMOTE;
      fi;
    # Config Environment Variables
    - heroku config:set NODE_ENV=$NODE_ENV -a $REVIEW_APP_NAME -r $REVIEW_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_ID=$AUTHO_MANAGEMENT_API_CLIENT_ID -a $REVIEW_APP_NAME -r $REVIEW_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_SECRET=$AUTHO_MANAGEMENT_API_CLIENT_SECRET -a $REVIEW_APP_NAME -r $REVIEW_REMOTE
    # Push to heroku reviewing app
    - git push $REVIEW_REMOTE HEAD:refs/heads/master
    # Push roles and permissions to Auth0 development tentant
    - npm run upload-permissions

staging_job:
  stage: staging:staging_app
  only:
    - master
  variables:
    NODE_ENV: staging
    AUTHO_DOMAIN: cah-staging.us.auth0.com
    AUTHO_AUDIENCE: https://cah-apollo-staging.herokuapp.com/graphql
    AUTHO_CUSTOM_CLAIMS_NAMESPACE: https://CAH-staging.com/
    AUTHO_MANAGEMENT_API_CLIENT_ID: '$STAGING_AUTHO_MANAGEMENT_API_CLIENT_ID'
    AUTHO_MANAGEMENT_API_CLIENT_SECRET: '$STAGING_AUTHO_MANAGEMENT_API_CLIENT_SECRET'
  script:
    # Create the Heroku pipeline if it doesn't already exists
    - |
      if ! heroku pipelines:info $HEROKU_APP_PIPELINE &> /dev/null; 
        then 
          # If the pipeline doesn't already exists create the heroku staging app
          heroku create $HEROKU_APP_STAGING -r $STAGING_REMOTE && 
          # Create the pipeline and add the staging app
          heroku pipelines:create $HEROKU_APP_PIPELINE -a $HEROKU_APP_STAGING -s staging && 
          # Create production app
          heroku create $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE && 
          # Add production app to the pipeline
          heroku pipelines:add $HEROKU_APP_PIPELINE -a $HEROKU_APP_PRODUCTION -s production;
        else
          # If the pipeline already exists only create the remote
          heroku git:remote -a $HEROKU_APP_STAGING -r $STAGING_REMOTE;
      fi;
    # Config Environment Variables
    - heroku config:set NODE_ENV=$NODE_ENV -a $HEROKU_APP_STAGING -r $STAGING_REMOTE
    - heroku config:set AUTHO_DOMAIN=$AUTHO_DOMAIN -a $HEROKU_APP_STAGING -r $STAGING_REMOTE
    - heroku config:set AUTHO_AUDIENCE=$AUTHO_AUDIENCE -a $HEROKU_APP_STAGING -r $STAGING_REMOTE
    - heroku config:set AUTHO_CUSTOM_CLAIMS_NAMESPACE=$AUTHO_CUSTOM_CLAIMS_NAMESPACE -a $HEROKU_APP_STAGING -r $STAGING_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_ID=$AUTHO_MANAGEMENT_API_CLIENT_ID -a $HEROKU_APP_STAGING -r $STAGING_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_SECRET=$AUTHO_MANAGEMENT_API_CLIENT_SECRET -a $HEROKU_APP_STAGING -r $STAGING_REMOTE
    # Push to heroku staging app
    - git push $STAGING_REMOTE HEAD:refs/heads/master
    # Check if it is a merge to master
    - |       
      if [[ $CI_COMMIT_TITLE =~ ^[Mm]erge\ branch\ [\"\'].+[\"\']\ into\ [\"\']master[\"\']$ ]];
        then 
          # Get the sorce branch name 
          [[ $CI_COMMIT_TITLE =~ [[:space:]][\"\'].+[\"\'][[:space:]] ]] && 
          SOURCE_BRANCH=${BASH_REMATCH:2:-2};
        else 
          echo $'This isn\'t a merge request to master';
      fi;
    # Remove the reviewing app if already exists
    - APP_TO_DELETE=$APP_NAME-$SOURCE_BRANCH
    - |      
      if [ ! -z $SOURCE_BRANCH ] && 
      heroku apps:info -a $APP_TO_DELETE &> /dev/null;
        then 
          heroku apps:destroy $APP_TO_DELETE -c $APP_TO_DELETE;
      fi;
    # Push roles and permissions to Auth0 for staging tenant
    - npm run upload-permissions

staging_default_dev_job:
  stage: staging:default_dev_app
  only:
    - master
  variables:
    NODE_ENV: development
    AUTHO_MANAGEMENT_API_CLIENT_ID: '$DEVELOPMENT_AUTHO_MANAGEMENT_API_CLIENT_ID'
    AUTHO_MANAGEMENT_API_CLIENT_SECRET: '$DEVELOPMENT_AUTHO_MANAGEMENT_API_CLIENT_SECRET'
  script:
    - echo 'Staging default development app job'
    - |
      if ! heroku pipelines:info $HEROKU_APP_PIPELINE &> /dev/null; 
        then 
          # Create default development app. This app is a staging replica for 
          # development environment
          heroku create $HEROKU_DEFAULT_DEV_APP -r $DEVELOPMENT_REMOTE &&
          # Add development app to the pipeline
          heroku pipelines:add $HEROKU_APP_PIPELINE -a $HEROKU_DEFAULT_DEV_APP -s development;
        else
          # If the pipeline already exists only create the remote
          heroku git:remote -a $HEROKU_DEFAULT_DEV_APP -r $DEVELOPMENT_REMOTE;
      fi;
    # Config Environment Variables
    - heroku config:set NODE_ENV=$NODE_ENV -a $HEROKU_DEFAULT_DEV_APP -r $DEVELOPMENT_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_ID=$AUTHO_MANAGEMENT_API_CLIENT_ID -a $HEROKU_DEFAULT_DEV_APP -r $DEVELOPMENT_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_SECRET=$AUTHO_MANAGEMENT_API_CLIENT_SECRET -a $HEROKU_DEFAULT_DEV_APP -r $DEVELOPMENT_REMOTE
    # Push to default development app
    - git push $DEVELOPMENT_REMOTE HEAD:refs/heads/master
    # Push roles and permissions to auth0 for development tentant
    - npm run upload-permissions

production_job:
  stage: production
  only:
    - tags
  only:
    - /^prod-.*$/
  variables:
    NODE_ENV: production
    AUTHO_AUDIENCE: https://cah-apollo.herokuapp.com/graphql
    AUTHO_CUSTOM_CLAIMS_NAMESPACE: https://cah-seven.vercel.app/
    AUTHO_DOMAIN: cards-against-humanity.us.auth0.com
    AUTHO_MANAGEMENT_API_CLIENT_ID: '$PRODUCTION_AUTHO_MANAGEMENT_API_CLIENT_ID'
    AUTHO_MANAGEMENT_API_CLIENT_SECRET: '$PRODUCTION_AUTHO_MANAGEMENT_API_CLIENT_SECRET'
  script:
    # --- Upload Production APP ---
    # Create a new Heroku app and his remote if it doesn't already exist
    - |
      if ! heroku apps:info -a $HEROKU_APP_PRODUCTION &> /dev/null; 
        then 
          heroku create $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE && 
          # Add the app to pipeline in development stage
          heroku pipelines:add $HEROKU_APP_PIPELINE -a $HEROKU_APP_PRODUCTION -s production;
        else 
          heroku git:remote -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE;
      fi;
    # Config Environment Variables
    - heroku config:set NODE_ENV=$NODE_ENV -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE
    - heroku config:set AUTHO_AUDIENCE=$AUTHO_AUDIENCE -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE
    - heroku config:set AUTHO_CUSTOM_CLAIMS_NAMESPACE=$AUTHO_CUSTOM_CLAIMS_NAMESPACE -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE
    - heroku config:set AUTHO_DOMAIN=$AUTHO_DOMAIN -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_ID=$AUTHO_MANAGEMENT_API_CLIENT_ID -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE
    - heroku config:set AUTHO_MANAGEMENT_API_CLIENT_SECRET=$AUTHO_MANAGEMENT_API_CLIENT_SECRET -a $HEROKU_APP_PRODUCTION -r $PRODUCTION_REMOTE
    # Push to heroku reviewing app
    - git push $PRODUCTION_REMOTE HEAD:refs/heads/master
    # Push roles and permissions to Auth0 development tentant
    - npm run upload-permissions
